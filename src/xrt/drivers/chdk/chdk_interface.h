// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Interface to direct OSVR HDK driver code.
 * @author Jakob Bornecrantz <jakob@collabora.com>
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 * @ingroup drv_hdk
 */

#pragma once

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif


/*!
 * @defgroup drv_hdk HDK Driver
 * @ingroup drv
 *
 * @brief Driver for the HDK HMD.
 */

//#define CHDK_VID 0x17ef
//#define CHDK_PID 0x6044

// demo glass
//#define CHDK_VID 0x16c0
//#define CHDK_PID 0x05df

//1b4f:9206
#define CHDK_VID 0x1b4f
#define CHDK_PID 0x9206

//2341:0043 Arduino SA Uno R3 (CDC ACM)
//#define CHDK_VID 0x2341
//#define CHDK_PID 0x0043

// T480s internal device
//#define CHDK_VID 0x13d3
//#define CHDK_PID 0x56a6


/*!
 * Probing function for HDK devices.
 *
 * @ingroup drv_hdk
 */
int
chdk_found(struct xrt_prober *xp,
          struct xrt_prober_device **devices,
          size_t num_devices,
          size_t index,
          struct xrt_device **out_xdev);

/*!
 * @dir drivers/hdk
 *
 * @brief @ref drv_hdk files.
 */


#ifdef __cplusplus
}
#endif
