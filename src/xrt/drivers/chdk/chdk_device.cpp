// Copyright 2019, Collabora, Ltd.
// Copyright 2014, Kevin M. Godby
// Copyright 2014-2018, Sensics, Inc.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Driver for an OSVR Hacker Dev Kit device.
 *
 * Based in part on the corresponding VRPN driver,
 * available under BSL-1.0.
 *
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 * @author Kevin M. Godby <kevin@godby.org>
 * @ingroup drv_hdk
 */


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <type_traits>

#include "xrt/xrt_device.h"
#include "math/m_api.h"
#include "util/u_debug.h"
#include "util/u_misc.h"
#include "util/u_device.h"
#include "util/u_time.h"
#include "os/os_hid.h"
#include "util/u_distortion_mesh.h"

#include "chdk_device.h"
#include <iostream>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <cmath>
#include <sys/ioctl.h>

int
set_interface_attribs (int fd, int speed, int parity)
{
    struct termios tty;
    if (tcgetattr (fd, &tty) != 0)
    {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    cfmakeraw(&tty);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

void
set_blocking (int fd, int should_block)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
//        error_message ("error %d from tggetattr", errno);
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
//        error_message ("error %d setting term attributes", errno);
        printf("Error setting term attributes tcgetattr: %s\n", strerror(errno));

}
char teapotPacket[14];// = new char[14];  // InvenSense Teapot packet
char teapotPacketHID[16];// for HID to read InvenSense Teapot packet
int serialCount = 0;                 // current packet byte position
int synced = 0;
int interval = 0;

float q[4];// = new float[4];
int fd;

int imu_init(){
    char portname[] = "/dev/ttyACM0";

    fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        printf("Error opening %s: %s\n", portname, strerror(errno));
        return 0;
    }

//    set_interface_attribs (fd, B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
    set_interface_attribs (fd, B230400, 0);  // set speed to 230,400 bps, 8n1 (no parity)
    set_blocking (fd, 0);

    write (fd, "r\n", 2);           // send reset character to reset firmware in Arduino

    usleep ((7 + 25) * 100);             // sleep enough to transmit the 7 plus
    // receive 25:  approx 100 uS per char transmit
    usleep (3000000);             // sleep enough
    write (fd, "a\n", 2);           // send any character to kick start
    usleep (3000000);             // sleep enough
    return 1;
}

void imu_read() {

    int available_bytes;
    ioctl(fd, FIONREAD, &available_bytes);
    printf("available bytes = %d\n", available_bytes);

    if (available_bytes == 0)
        return;

    char arr[available_bytes];
    int ret = read (fd, arr, sizeof(arr));
    if (ret ==0)
        return ;

    char ch[1];
    for (int i=0; i< sizeof(arr); i++)
    {
        ch[0] = arr[i];
        printf("%c", ch[0]);

        if (synced == 0 && ch[0] != '$') { //ascii = 0x24
            std::cout << "return 1" << std::endl;
            //return ;
            continue;
        }
        synced = 1;
        if ((serialCount == 1 && ch[0] != 2)
            || (serialCount == 12 && ch[0] != '\r')
            || (serialCount == 13 && ch[0] != '\n'))  {
            serialCount = 0;
            synced = 0;
            std::cout << "serialCount ==" <<serialCount << "&& 2, r ,n != " << ch[0] << std::endl;
            return ;
            //continue;
        }
        if (serialCount > 0 || ch[0] == '$') {
            teapotPacket[serialCount++] = (char) ch[0];
            std::cout << "fill teapot[" << serialCount << "]" << std::endl;
            if (serialCount == 14) {
                serialCount = 0; // restart packet byte position

//                std::cout << "teapotPacket:";
//                for (int i =2; i<10; i++)
//                    std::cout << " 0x" << std::hex << (0x00ff & (int)(teapotPacket[i])) ;
//                std::cout << std::endl;

                // get quaternion from data packet
                q[0] = (((teapotPacket[2] & 0x00ff) << 8) | (teapotPacket[3] & 0x00ff)) / 16384.0f;
                q[1] = (((teapotPacket[4] & 0x00ff) << 8) | (teapotPacket[5] & 0x00ff)) / 16384.0f;
                q[2] = (((teapotPacket[6] & 0x00ff) << 8) | (teapotPacket[7] & 0x00ff)) / 16384.0f;
                q[3] = (((teapotPacket[8] & 0x00ff) << 8) | (teapotPacket[9] & 0x00ff)) / 16384.0f;
                for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];

                std::cout << "QQQq:\t" << std::fixed << round(q[0]*100.0f)/100.0f << "\t" << round(q[1]*100.0f)/100.0f << "\t" << round(q[2]*100.0f)/100.0f << "\t" << round(q[3]*100.0f)/100.0f << std::endl;

            }
        }
    }

}

static void
chdk_device_destroy(struct xrt_device *xdev)
{
	struct chdk_device *hd = chdk_device(xdev);

	if (hd->dev != NULL) {
		os_hid_destroy(hd->dev);
		hd->dev = NULL;
	}

	free(hd);
}

static void
chdk_device_update_inputs(struct xrt_device *xdev,
                         struct time_state *timekeeping)
{
	// Empty
}

static void
xchdk_device_get_tracked_pose(struct xrt_device *xdev,
                             enum xrt_input_name name,
                             struct time_state *timekeeping,
                             int64_t *out_timestamp,
                             struct xrt_space_relation *out_relation) {
    struct chdk_device *hd = chdk_device(xdev);

    if (name != XRT_INPUT_GENERIC_HEAD_POSE) {
        CHDK_ERROR(hd, "unknown input name");
        return;
    }

    int64_t now = time_state_get_now(timekeeping);

    float ftemp = 0.0;
    //uint8_t buffer[32];
    uint8_t buffer[8];
    // Adjusting for latency - 14ms, found empirically.
//    now -= 14000000;
    now -= 1000000;
    *out_timestamp = now;
    //uint8_t *buf = &(buffer[0]);


    struct xrt_quat quat;


//***********************************************
    imu_read();
    quat.w = q[0];
    quat.x = q[1];
    quat.y = q[3]; // opexr coordinate y is up to sky, mpu6050 y is face to front!!!
    quat.z = q[2] * -1; // opexr coordinate z is face to back, mpu6050 z is up to sky!!!
#ifdef NOTUSE
    char ch[1];
    int ret = read (fd, ch, sizeof(char));
    if (ret ==0) return ;
        printf("%c", ch[0]);

    if (synced == 0 && ch[0] != '$') {
        return ;
    }
    synced = 1;
    if ((serialCount == 1 && ch[0] != 2)
        || (serialCount == 12 && ch[0] != '\r')
        || (serialCount == 13 && ch[0] != '\n'))  {
        serialCount = 0;
        synced = 0;
        //           std::cout << "serialCount ==" <<serialCount << "&& ch != " << (char)ch << std::endl;
        return ;
    }
    if (serialCount > 0 || ch[0] == '$') {
        teapotPacket[serialCount++] = (char) ch[0];
//        std::cout << "fill teapot[" << serialCount << "]" << std::endl;
        if (serialCount == 14) {
            serialCount = 0; // restart packet byte position

//                std::cout << "teapotPacket:";
//                for (int i =2; i<10; i++)
//                    std::cout << " 0x" << std::hex << (0x00ff & (int)(teapotPacket[i])) ;
//                std::cout << std::endl;

            // get quaternion from data packet
            q[0] = (((teapotPacket[2] & 0x00ff) << 8) | (teapotPacket[3] & 0x00ff)) / 16384.0f;
            q[1] = (((teapotPacket[4] & 0x00ff) << 8) | (teapotPacket[5] & 0x00ff)) / 16384.0f;
            q[2] = (((teapotPacket[6] & 0x00ff) << 8) | (teapotPacket[7] & 0x00ff)) / 16384.0f;
            q[3] = (((teapotPacket[8] & 0x00ff) << 8) | (teapotPacket[9] & 0x00ff)) / 16384.0f;
            for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];

            std::cout << "QQQq:\t" << std::fixed << round(q[0]*100.0f)/100.0f << "\t" << round(q[1]*100.0f)/100.0f << "\t" << round(q[2]*100.0f)/100.0f << "\t" << round(q[3]*100.0f)/100.0f << std::endl;

        }
    }
#endif
//********************************************
#ifdef NOFAKE
    static float QW = 1.0;
    static float temp {0.0};
    static float step {0.005}, step2 {0.005};

    QW -= step;
    quat.w = QW;
    quat.x = 0;
    quat.z = 0; //
    temp += step2;
    quat.y = temp; //

    if ((QW < -1.0) || QW > 1.0)
        step *= -1;
    if ( (temp <= 0.0) || (temp >= 1.0))
        step2 *= -1;
#endif //NOFAKE

    out_relation->pose.orientation = quat;
    out_relation->relation_flags = xrt_space_relation_flags(
            XRT_SPACE_RELATION_ORIENTATION_VALID_BIT |
            //	    XRT_SPACE_RELATION_ANGULAR_VELOCITY_VALID_BIT |
            XRT_SPACE_RELATION_ORIENTATION_TRACKED_BIT);


    CHDK_SPEW(hd, "GET_TRACKED_POSE (%f, %f, %f, %f)", quat.w, quat.x, quat.y, quat.z );


}

void imu_readHID(struct chdk_device *hd) {

    uint8_t arr[16];

//    auto bytesRead = os_hid_read(hd->dev, arr, sizeof(arr), 0);
//    if (bytesRead == -1) {
//        if (!hd->disconnect_notified) {
//            fprintf(stderr,
//                    "%s: HDK appeared to disconnect. Please quit, "
//                    "reconnect, and try again.\n",
//                    __func__);
//            hd->disconnect_notified = true;
//        }
////        out_relation->relation_flags = XRT_SPACE_RELATION_BITMASK_NONE;
//        return;
//    }
//    if (bytesRead!=16) {
//        printf( "WAIT!\n");
//        usleep(5000);             // pause for hid data come in
//        return;
//    }

    while(os_hid_read(hd->dev, arr, sizeof(arr), 0) != 16)
        usleep(1);
    while(os_hid_read(hd->dev, arr, sizeof(arr), 0) == 16) //read all for flushing
        usleep(1);

//    for (int i=0; i< sizeof(arr); i++)
//        printf("0x%02X ", arr[i]);
//    printf( "\n");


    char ch[1];
    for (int i=0; i< sizeof(arr); i++)
    {
        ch[0] = arr[i];
//        printf("%c", ch[0]);

        if (synced == 0 && ch[0] != '$') {//ascii = 0x24
            //return ;
            continue;
        }
        synced = 1;
        if ((serialCount == 1 && ch[0] != 2)
            || (serialCount == 14 && ch[0] != '\r')
            || (serialCount == 15 && ch[0] != '\n'))  {
            serialCount = 0;
            synced = 0;
            //           std::cout << "serialCount ==" <<serialCount << "&& ch != " << (char)ch << std::endl;
            return ;
            //continue;
        }
        if (serialCount > 0 || ch[0] == '$') {
            teapotPacketHID[serialCount++] = (char) ch[0];
//            std::cout << "fill teapot[" << serialCount << "]" << std::endl;
            if (serialCount == 16) {
                serialCount = 0; // restart packet byte position

//                std::cout << "teapotPacket:";
//                for (int i =2; i<10; i++)
//                    std::cout << " 0x" << std::hex << (0x00ff & (int)(teapotPacket[i])) ;
//                std::cout << std::endl;

                // get quaternion from data packet
                q[0] = (((teapotPacketHID[2] & 0x00ff) << 8) | (teapotPacketHID[3] & 0x00ff)) / 16384.0f;
                q[1] = (((teapotPacketHID[4] & 0x00ff) << 8) | (teapotPacketHID[5] & 0x00ff)) / 16384.0f;
                q[2] = (((teapotPacketHID[6] & 0x00ff) << 8) | (teapotPacketHID[7] & 0x00ff)) / 16384.0f;
                q[3] = (((teapotPacketHID[8] & 0x00ff) << 8) | (teapotPacketHID[9] & 0x00ff)) / 16384.0f;
                for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];

//                std::cout << "QQQq:\t" << std::fixed << round(q[0]*100.0f)/100.0f << "\t" << round(q[1]*100.0f)/100.0f << "\t" << round(q[2]*100.0f)/100.0f << "\t" << round(q[3]*100.0f)/100.0f << std::endl;

            }
        }
    }

}


static void
chdk_device_get_tracked_pose(struct xrt_device *xdev,
                            enum xrt_input_name name,
                            struct time_state *timekeeping,
                            int64_t *out_timestamp,
                            struct xrt_space_relation *out_relation) {
    struct chdk_device *hd = chdk_device(xdev);

    if (name != XRT_INPUT_GENERIC_HEAD_POSE) {
        CHDK_ERROR(hd, "unknown input name");
        return;
    }

    int64_t now = time_state_get_now(timekeeping);

    float ftemp = 0.0;
    uint8_t buffer[16];

    // Adjusting for latency - 14ms, found empirically.
    now -= 14000000;
    *out_timestamp = now;
    //uint8_t *buf = &(buffer[0]);


    struct xrt_quat quat;

//***********************************************
    while (0) {
        auto bytesRead = os_hid_read(hd->dev, buffer, sizeof(buffer), 0);
        if (bytesRead!=16) {
            usleep(50000);             // sleep enough
            continue;
        }
        //printf( "got %d bytes\n", bytesRead);
        for (int i=0; i< sizeof(buffer); i++)
            printf("0x%02X ", buffer[i]);
        printf( "\n");

    }

    imu_readHID(hd);
//IMU mounted on demo glass
    quat.w = q[0];
    quat.x = q[2]; // opexr coordinate x is move to right, demo glass mpu6050 y is move to right!!!
    quat.y = q[3]; // opexr coordinate y is up to sky, demo glass mpu6050 z is up to sky!!!
    quat.z = q[1]; // opexr coordinate z is face to our head, demo glass x is face to our head

// CRI想轉90度把MPU6050的PCB立起來.
//    quat.x = q[2]; // opexr coordinate x is move to right, demo glass mpu6050 y is move to right!!!
//    quat.y = q[1]; // opexr coordinate y is up to sky, demo glass mpu6050 z is up to sky!!!
//    quat.z = q[3] * -1; // opexr coordinate z is face to our head, demo glass x is face to our head

    out_relation->pose.orientation = quat;
    out_relation->relation_flags = xrt_space_relation_flags(
            XRT_SPACE_RELATION_ORIENTATION_VALID_BIT |
            //	    XRT_SPACE_RELATION_ANGULAR_VELOCITY_VALID_BIT |
            XRT_SPACE_RELATION_ORIENTATION_TRACKED_BIT);


	CHDK_SPEW(hd, "GET_TRACKED_POSE (%f, %f, %f, %f)", quat.w, quat.x, quat.y, quat.z );
}

static void
chdk_device_get_view_pose(struct xrt_device *xdev,
                         struct xrt_vec3 *eye_relation,
                         uint32_t view_index,
                         struct xrt_pose *out_pose)
{
	struct xrt_pose pose = {{0.0f, 0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}};
	bool adjust = view_index == 0;

	pose.position.x = eye_relation->x / 2.0f;
	pose.position.y = eye_relation->y / 2.0f;
	pose.position.z = eye_relation->z / 2.0f;

	// Adjust for left/right while also making sure there aren't any -0.f.
	if (pose.position.x > 0.0f && adjust) {
		pose.position.x = -pose.position.x;
	}
	if (pose.position.y > 0.0f && adjust) {
		pose.position.y = -pose.position.y;
	}
	if (pose.position.z > 0.0f && adjust) {
		pose.position.z = -pose.position.z;
	}

	*out_pose = pose;
}

#define HDK_DEBUG_INT(hd, name, val) HDK_DEBUG(hd, "\t%s = %u", name, val)

#define HDK_DEBUG_MM(hd, name, val)                                            \
	HDK_DEBUG(hd, "\t%s = %i.%02imm", name, (int32_t)(val * 1000.f),       \
	          abs((int32_t)(val * 100000.f)) % 100)

#define HDK_DEBUG_ANGLE(hd, name, val)                                         \
	HDK_DEBUG(hd, "\t%s = %f (%i)", name, val,                             \
	          (int32_t)(val * (180 / M_PI)))

#define HDK_DEBUG_MAT2X2(hd, name, rot)                                        \
	HDK_DEBUG(hd, "\t%s = {%f, %f} {%f, %f}", name, rot.v[0], rot.v[1],    \
	          rot.v[2], rot.v[3])

struct chdk_device *
chdk_device_create(struct os_hid_device *dev,
                  enum HDK_VARIANT variant,
                  bool print_spew,
                  bool print_debug)
{
	enum u_device_alloc_flags flags = (enum u_device_alloc_flags)(
	    U_DEVICE_ALLOC_HMD | U_DEVICE_ALLOC_TRACKING_NONE);
	struct chdk_device *hd =
	    U_DEVICE_ALLOCATE(struct chdk_device, flags, 1, 0);

	hd->base.hmd->blend_mode = XRT_BLEND_MODE_OPAQUE;
	hd->base.update_inputs = chdk_device_update_inputs;
	hd->base.get_tracked_pose = chdk_device_get_tracked_pose;
	hd->base.get_view_pose = chdk_device_get_view_pose;
	hd->base.destroy = chdk_device_destroy;
	hd->base.inputs[0].name = XRT_INPUT_GENERIC_HEAD_POSE;
	hd->base.name = XRT_DEVICE_GENERIC_HMD;
	hd->dev = dev;
	hd->print_spew = print_spew;
	hd->print_debug = print_debug;

	snprintf(hd->base.str, XRT_DEVICE_NAME_LEN, "Coretronic CHDK-family Device");

	if (variant == HDK_UNKNOWN) {
		CHDK_ERROR(hd, "Don't know which CHDK variant this is.");
		chdk_device_destroy(&hd->base);
		return NULL;
	}

	// PSVR so far so good
if (0) { //1 for HDK style or 0 for PSVR style of view settting
    double hFOV;
    double vFOV;
    double hCOP = 0.5;
    double vCOP = 0.5;


    // Mesh distortion (ideally)
    hFOV = vFOV = 40.0;


    constexpr double DEGREES_TO_RADIANS = M_PI / 180.0;
    {
        /* right eye */
        math_compute_fovs(1.0, hCOP, hFOV * DEGREES_TO_RADIANS, 1, vCOP,
                          vFOV * DEGREES_TO_RADIANS,
                          &hd->base.hmd->views[1].fov);
    }
    {
        /* left eye - just mirroring right eye now */
        hd->base.hmd->views[0].fov.angle_up =
                hd->base.hmd->views[1].fov.angle_up;
        hd->base.hmd->views[0].fov.angle_down =
                hd->base.hmd->views[1].fov.angle_down;

        hd->base.hmd->views[0].fov.angle_left =
                -hd->base.hmd->views[1].fov.angle_right;
        hd->base.hmd->views[0].fov.angle_right =
                -hd->base.hmd->views[1].fov.angle_left;
    }

    hd->base.hmd->screens[0].nominal_frame_interval_ns =
            time_s_to_ns(1.0f / 60.0f);
    constexpr int panel_w = 1280;
    constexpr int panel_h = 720;
    // Padding needed horizontally per side.
    constexpr int vert_padding = 0;

    // clang-format off
    // Main display.
    hd->base.hmd->screens[0].w_pixels = panel_w * 2;
    hd->base.hmd->screens[0].h_pixels = panel_h;

    // Left
    hd->base.hmd->views[0].display.w_pixels = panel_w;
    hd->base.hmd->views[0].display.h_pixels = panel_h;
    hd->base.hmd->views[0].viewport.x_pixels = 0;
    hd->base.hmd->views[0].viewport.y_pixels = vert_padding;
    hd->base.hmd->views[0].viewport.w_pixels = panel_w;
    hd->base.hmd->views[0].viewport.h_pixels = panel_h;
    hd->base.hmd->views[0].rot = u_device_rotation_ident;

    // Right
    hd->base.hmd->views[1].display.w_pixels = panel_w;
    hd->base.hmd->views[1].display.h_pixels = panel_h;
    hd->base.hmd->views[1].viewport.x_pixels = panel_w;
    hd->base.hmd->views[1].viewport.y_pixels = vert_padding;
    hd->base.hmd->views[1].viewport.w_pixels = panel_w;
    hd->base.hmd->views[1].viewport.h_pixels = panel_h;
    hd->base.hmd->views[1].rot = u_device_rotation_ident;
    // clang-format on

    // Distortion
    // "None" is correct or at least acceptable for 1.2.
    // We have coefficients for 1.3/1.4, though the mesh is better.
    // We only have a mesh for 2, so use "none" there until it's supported.
    hd->base.hmd->distortion.models = XRT_DISTORTION_MODEL_NONE;
    hd->base.hmd->distortion.preferred = XRT_DISTORTION_MODEL_NONE;
}
else
{

    if (0){ //add pantools effect or not
        struct u_panotools_values vals = {0};

        vals.distortion_k[0] = 0.75;
        vals.distortion_k[1] = -0.01;
        vals.distortion_k[2] = 0.75;
        vals.distortion_k[3] = 0.0;
        vals.distortion_k[4] = 3.8;
        vals.aberration_k[0] = 0.999;
        vals.aberration_k[1] = 1.008;
        vals.aberration_k[2] = 1.018;
        vals.scale = 1.2 * (1980 / 2.0f);
        vals.viewport_size.x = (1980 / 2.0f);
        vals.viewport_size.y = (1080);
        vals.lens_center.x = vals.viewport_size.x / 2.0;
        vals.lens_center.y = vals.viewport_size.y / 2.0;

        u_distortion_mesh_from_panotools(&vals, &vals, hd->base.hmd);
    }
        /*
         * Device setup as PSVR style
         * 2560x720     59.77 CHDK OK
         * 1280x720     60 CHDK OK
         * 1600x1280    60 DreamGlass
         * 2160x1200    90 OSVK HDK2
         */

        struct u_device_simple_info info;

    info.display.w_pixels = 2560;
    info.display.h_pixels = 720;
    info.display.w_meters = 0.13f;
    info.display.h_meters = 0.07f;
    info.lens_horizontal_separation_meters = 0.13f / 2.0f;
    info.lens_vertical_position_meters = 0.07f / 2.0f;
    info.views[0].fov = 85.0f * (M_PI / 180.0f);
    info.views[1].fov = 85.0f * (M_PI / 180.0f);
    hd->base.hmd->screens[0].nominal_frame_interval_ns =
            time_s_to_ns(1.0f / 60.0f);

    /*
     * Device setup from PSVR as sample
    */
//    info.display.w_pixels = 1920;
//    info.display.h_pixels = 1080;
//    info.display.w_meters = 0.13f;
//    info.display.h_meters = 0.07f;
//    info.lens_horizontal_separation_meters = 0.13f / 2.0f;
//    info.lens_vertical_position_meters = 0.07f / 2.0f;
//    info.views[0].fov = 85.0f * (M_PI / 180.0f);
//    info.views[1].fov = 85.0f * (M_PI / 180.0f);


    if (!u_device_setup_split_side_by_side(&hd->base, &info)) {
    CHDK_ERROR(hd, "Failed to setup basic device info");
    chdk_device_destroy(&hd->base);
    return NULL;
    }

} // PSVR

    if (hd->print_debug) {
        u_device_dump_config(&hd->base, __func__, hd->base.str);

    }

//    imu_init();

return hd;
}
